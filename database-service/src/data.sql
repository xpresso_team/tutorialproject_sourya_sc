CREATE DATABASE  IF NOT EXISTS `python_benchmark`;
USE `python_benchmark`;

CREATE TABLE IF NOT EXISTS `data`(
        `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `ts` timestamp default now(),
        `message` VARCHAR(255)
);
